import sortwords
import sys


def search_word(word, words_list):
    found = False
    for i in words_list:
        if sortwords.equal(word, i):
            index1 = words_list.index(i)
            found = True
            return index1

    if not found:
        raise Exception


def main():
    words_list = sys.argv[2:]
    word = sys.argv[1]

    if len(sys.argv) < 3:
        sys.exit("Se necesitan al menos dos palabras.")

    try:
        ordered_list = sortwords.sort(words_list)
        index = search_word(word, ordered_list)
        sortwords.show(ordered_list)
        print(index)
    except Exception:
        sys.exit("Palabra no encontrada en el sistema")


if __name__ == '__main__':
    main()
